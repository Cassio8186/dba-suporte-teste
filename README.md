# Teste para DBA Suporte

## Tarefas Realizadas

### Requisitos Funcionais

- [x] Verificar se todos
      os parâmetros obrigatórios do serviço
      foram preenchidos para transação.
- [x] Verificar se o cliente sendo cadastrado já não existe no banco. Se o cliente existir, deverá
      negar o cadastramento e informar que
      já existe um cliente com os dados
      informados no sistema.
- [x] deverá cadastrar os
      dados do usuário no banco de dados
      do sistema. O sistema deverá validar
      se o email é válido, se o telefone está
      no formato padrão internacional e se o
      CPF ou CNPJ é válido.
- [x] Deverá cadastrar a
      documentação informada pelo usuário
      na base de dados do sistema. Caso a
      documentação não esteja no formato
      PDF, o sistema deverá recusar os
      dados.
- [x] O UC deverá exportar para o cliente da
      requisição o resultado do
      processamento do caso de uso.

### Documentação de Web Service

[Link para documentação](https://dba-suporte-test-cassio.herokuapp.com/swagger-ui.html)

Nesta documentação encontram-se informações de como o Webservice deverá ser consumido e o que será retornado.

A tecnologia utilizada para documentação do Web Service foi **Swagger** pois além de documentar em tempo real a aplicação, permite que o usuário possa realizar testes diretamente no mesmo ambiente.

## Próximos Passos

- [ ] Criar testes para Serviço de Documentação
- [ ] Criar testes para Controller de Cliente
- [ ] Criar testes para Controller de documentação
- [ ] Aplicar limite de requisições por segundos para cada ip.
- [ ] Implementar autenticação com Spring Security
- [ ] Adicionar Liquibase para gerenciamento das migrations do banco de dados
