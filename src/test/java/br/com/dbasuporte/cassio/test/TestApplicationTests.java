package br.com.dbasuporte.cassio.test;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootApplication
@SpringBootTest
@ActiveProfiles("h2")
class TestApplicationTests {

	@Test
	void contextLoads() {
	}

}
