package br.com.dbasuporte.cassio.desafio.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import br.com.dbasuporte.cassio.desafio.exception.EntityAlreadyExistsException;
import br.com.dbasuporte.cassio.desafio.model.Cliente;
import br.com.dbasuporte.cassio.desafio.repository.ClienteRepository;

@SpringBootTest
@ActiveProfiles("h2")
class ClienteServiceTest {

	private Cliente cliente;

	private ClienteService clienteService;

	private ClienteRepository clienteRepository;

	@Autowired
	public ClienteServiceTest(ClienteService clienteService, ClienteRepository clienteRepository) {
		this.clienteService = clienteService;
		this.clienteRepository = clienteRepository;
	}

	@BeforeEach
	void setUp() throws Exception {
		this.cliente = Cliente.builder()
				.cpfCnpj("18071468045")
				.email("jorge@gmail.com")
				.nomeCompleto("Jorge Campos Roberto")
				.telefone("+5591548201546")
				.build();

	}

	@AfterEach
	void tearDown() throws Exception {
		cliente = null;
		this.clienteRepository.deleteAll();
	}

	@Test
	void testSave() {
		this.clienteService.save(cliente);

		Exception exception = assertThrows(EntityAlreadyExistsException.class, () -> {
			this.clienteService.save(cliente);
		});

		String expectedMessage = String.format("Entidade: %s já existe (Parametro: %s, Valor: %s)",
				Cliente.class.getSimpleName(), "cpf", cliente.getCpfCnpj());
		String actualMessage = exception.getMessage();
		assertEquals(expectedMessage, actualMessage);
	}

}
