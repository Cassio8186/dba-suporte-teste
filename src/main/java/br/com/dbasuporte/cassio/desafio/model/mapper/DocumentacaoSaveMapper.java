package br.com.dbasuporte.cassio.desafio.model.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.dbasuporte.cassio.desafio.model.Documentacao;
import br.com.dbasuporte.cassio.desafio.model.dto.DocumentacaoSaveDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = { ClienteMapper.class })
public interface DocumentacaoSaveMapper extends EntityMapper<Documentacao, DocumentacaoSaveDTO> {
	DocumentacaoSaveMapper INSTANCE = Mappers.getMapper(DocumentacaoSaveMapper.class);

	@Mapping(source = "cliente.cpfCnpj", target = "cpfCnpj")
	DocumentacaoSaveDTO toDTO(Documentacao entity);

	@InheritInverseConfiguration
	Documentacao toEntity(DocumentacaoSaveDTO dto);
}
