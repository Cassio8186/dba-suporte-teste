package br.com.dbasuporte.cassio.desafio.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ResponseDTO {
	private int codigoRetorno;

	@JsonInclude(value = Include.NON_NULL)
	private String mensagem;

	private ResponseDTO(Builder builder) {
		this.codigoRetorno = builder.codigoRetorno;
		this.mensagem = builder.mensagem;
	}

	public ResponseDTO() {
	}

	public int getCodigoRetorno() {
		return codigoRetorno;
	}

	public void setCodigoRetorno(int codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public String toString() {
		return "ResponseDTO [codigoRetorno=" + codigoRetorno + ", mensagem=" + mensagem + "]";
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private int codigoRetorno;
		private String mensagem;

		private Builder() {
		}

		public Builder codigoRetorno(int codigoRetorno) {
			this.codigoRetorno = codigoRetorno;
			return this;
		}

		public Builder mensagem(String mensagem) {
			this.mensagem = mensagem;
			return this;
		}

		public ResponseDTO build() {
			return new ResponseDTO(this);
		}
	}

}
