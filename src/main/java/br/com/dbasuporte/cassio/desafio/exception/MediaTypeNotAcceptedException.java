package br.com.dbasuporte.cassio.desafio.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MediaTypeNotAcceptedException extends RuntimeException {

	private static final long serialVersionUID = -3476814153748248178L;
	private static final Logger log = LoggerFactory.getLogger(MediaTypeNotAcceptedException.class);

	public MediaTypeNotAcceptedException(String fileExtension, String allowedExtension) {
		super(errorMessage(fileExtension, allowedExtension));
		log.error(errorMessage(fileExtension, allowedExtension));
	}

	private static String errorMessage(String fileExtension, String allowedExtension) {
		return String.format("Extensão %s não permitida, utilize a extensão %s", fileExtension, allowedExtension);
	}

}
