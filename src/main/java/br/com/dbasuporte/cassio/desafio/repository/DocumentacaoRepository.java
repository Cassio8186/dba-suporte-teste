package br.com.dbasuporte.cassio.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.dbasuporte.cassio.desafio.model.Documentacao;

@Repository
public interface DocumentacaoRepository extends JpaRepository<Documentacao, Integer> {

}
