package br.com.dbasuporte.cassio.desafio.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.dbasuporte.cassio.desafio.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

	Boolean existsByCpfCnpj(String cpfCnpj);

	Optional<Cliente> findByCpfCnpj(String cpfCnpj);
}
