package br.com.dbasuporte.cassio.desafio.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3476814153748248178L;
	private static final Logger log = LoggerFactory.getLogger(EntityNotFoundException.class);

	public EntityNotFoundException(Class<?> entity, String param, Object value) {
		super(errorMessage(entity, param, value));
		log.error(errorMessage(entity, param, value));
	}

	private static String errorMessage(Class<?> entity, String param, Object value) {
		return String.format("Entidade: %s não encontrada (Parametro: %s, Valor: %s)", entity.getSimpleName(), param,
				value);
	}

}
