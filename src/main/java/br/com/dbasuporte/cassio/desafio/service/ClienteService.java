package br.com.dbasuporte.cassio.desafio.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbasuporte.cassio.desafio.exception.EntityAlreadyExistsException;
import br.com.dbasuporte.cassio.desafio.exception.EntityNotFoundException;
import br.com.dbasuporte.cassio.desafio.model.Cliente;
import br.com.dbasuporte.cassio.desafio.repository.ClienteRepository;

@Service
@Transactional
public class ClienteService {

	private ClienteRepository clienteRepository;

	@Autowired
	public ClienteService(ClienteRepository clienteRepository) {
		this.clienteRepository = clienteRepository;
	}

	public void save(Cliente cliente) {
		throwErrorIfClienteExists(cliente.getCpfCnpj());
		this.clienteRepository.save(cliente);
	}

	public Cliente findByCpfCnpj(String cpfCnpj) {
		Optional<Cliente> optionalCliente = this.clienteRepository.findByCpfCnpj(cpfCnpj);
		if (!optionalCliente.isPresent()) {
			throw new EntityNotFoundException(Cliente.class, "cpf-cnpj", cpfCnpj);
		}
		return optionalCliente.get();
	}

	private void throwErrorIfClienteExists(String cpfCnpj) {
		Boolean clientExists = this.clienteRepository.existsByCpfCnpj(cpfCnpj);
		if (clientExists) {
			throw new EntityAlreadyExistsException(Cliente.class, "cpf", cpfCnpj);
		}
	}

	public void throwErrorIfClienteNotExists(String cpfCnpj) {
		Boolean clientExists = this.clienteRepository.existsByCpfCnpj(cpfCnpj);
		if (!clientExists) {
			throw new EntityNotFoundException(Cliente.class, "cpf", cpfCnpj);
		}
	}

}
