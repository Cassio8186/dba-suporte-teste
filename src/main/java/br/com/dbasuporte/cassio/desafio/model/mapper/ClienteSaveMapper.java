package br.com.dbasuporte.cassio.desafio.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.dbasuporte.cassio.desafio.model.Cliente;
import br.com.dbasuporte.cassio.desafio.model.dto.ClienteSaveDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {})
public interface ClienteSaveMapper extends EntityMapper<Cliente, ClienteSaveDTO> {

	ClienteSaveMapper INSTANCE = Mappers.getMapper(ClienteSaveMapper.class);
}
