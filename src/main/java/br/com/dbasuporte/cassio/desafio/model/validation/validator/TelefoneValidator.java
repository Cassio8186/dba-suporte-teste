package br.com.dbasuporte.cassio.desafio.model.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.dbasuporte.cassio.desafio.model.validation.annotation.Telefone;

/**
 * 
 * @author Clairton Luz - clairton.c.l@gmail.com
 * 
 *         <para>
 * 
 *         <a href="https://clairtonluz.github.io/blog/2014/07/2014072299.html">
 *         fonte </a>
 * 
 *         </para>
 */
public class TelefoneValidator implements ConstraintValidator<Telefone, String> {

	@Override
	public void initialize(Telefone constraintAnnotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value == null || value.isEmpty() || isTelefone(value);
	}

	private boolean isTelefone(String telefone) {
		String telefonePattern = "\\+[0-9]{12,13}";
		return telefone.matches(telefonePattern);
	}

}