package br.com.dbasuporte.cassio.desafio.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbasuporte.cassio.desafio.model.Cliente;
import br.com.dbasuporte.cassio.desafio.model.dto.ClienteSaveDTO;
import br.com.dbasuporte.cassio.desafio.model.dto.ResponseDTO;
import br.com.dbasuporte.cassio.desafio.model.mapper.ClienteSaveMapper;
import br.com.dbasuporte.cassio.desafio.service.ClienteService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin
public class ClienteController {
	private Logger log = LoggerFactory.getLogger(ClienteController.class);

	private ClienteService clienteService;

	@Autowired
	public ClienteController(ClienteService clienteService) {
		this.clienteService = clienteService;

	}

	@PostMapping(value = "/api/v1/clientes", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> saveCliente(@Valid @RequestBody ClienteSaveDTO clienteSaveDTO) {
		log.info("Salvando cliente: " + clienteSaveDTO.toString());
		Cliente cliente = ClienteSaveMapper.INSTANCE.toEntity(clienteSaveDTO);
		this.clienteService.save(cliente);

		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(200)
				.build();

		return ResponseEntity.ok(response);

	}

}
