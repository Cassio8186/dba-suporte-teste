package br.com.dbasuporte.cassio.desafio.controller;

import java.io.IOException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.dbasuporte.cassio.desafio.exception.MediaTypeNotAcceptedException;
import br.com.dbasuporte.cassio.desafio.model.Documentacao;
import br.com.dbasuporte.cassio.desafio.model.dto.DocumentacaoSaveDTO;
import br.com.dbasuporte.cassio.desafio.model.dto.ResponseDTO;
import br.com.dbasuporte.cassio.desafio.model.mapper.DocumentacaoSaveMapper;
import br.com.dbasuporte.cassio.desafio.service.DocumentacaoService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin
@Validated
public class DocumentacaoController {
	private Logger log = LoggerFactory.getLogger(DocumentacaoController.class);

	private DocumentacaoService documentacaoService;

	@Autowired
	public DocumentacaoController(DocumentacaoService documentacaoService) {
		this.documentacaoService = documentacaoService;

	}

	@PostMapping(value = "/api/v1/documentacoes/{tipo-documento}/{cpf-cnpj-cliente}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<ResponseDTO> saveDocumentacao(@RequestParam("file") MultipartFile file,
			@PathVariable("tipo-documento") String tipoDocumento,
			@PathVariable("cpf-cnpj-cliente") String cpfCnpjCliente)
			throws IOException {

		DocumentacaoSaveDTO documentacaoSaveDTO = DocumentacaoSaveDTO.builder()
				.cpfCnpj(cpfCnpjCliente)
				.tipoDocumento(tipoDocumento)
				.build();

		validateDocumentacaoSaveDTO(documentacaoSaveDTO);
		validateFileMediaType(file);

		log.info("Salvando documentacao: " + documentacaoSaveDTO.toString());

		Documentacao documentacao = DocumentacaoSaveMapper.INSTANCE.toEntity(documentacaoSaveDTO);

		documentacao.setDocumento(file.getBytes());

		this.documentacaoService.save(documentacao);

		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(200)
				.build();

		return ResponseEntity.ok(response);

	}

	/**
	 * @param file
	 * @throws MediaTypeNotAcceptedException
	 */
	private void validateFileMediaType(MultipartFile file) {
		if (!file.getContentType().equals(MediaType.APPLICATION_PDF_VALUE)) {
			throw new MediaTypeNotAcceptedException(file.getContentType(), MediaType.APPLICATION_PDF_VALUE);
		}
	}

	/**
	 * @throws ConstraintViolationException
	 */
	private void validateDocumentacaoSaveDTO(@Valid DocumentacaoSaveDTO documentacaoSaveDTO) {
		log.info(documentacaoSaveDTO.toString() + " valido");
	}
}
