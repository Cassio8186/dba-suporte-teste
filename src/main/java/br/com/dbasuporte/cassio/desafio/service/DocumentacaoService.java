package br.com.dbasuporte.cassio.desafio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbasuporte.cassio.desafio.model.Cliente;
import br.com.dbasuporte.cassio.desafio.model.Documentacao;
import br.com.dbasuporte.cassio.desafio.repository.DocumentacaoRepository;

@Service
@Transactional
public class DocumentacaoService {

	private DocumentacaoRepository documentacaoRepository;
	private ClienteService clienteService;

	@Autowired
	public DocumentacaoService(DocumentacaoRepository documentacaoRepository, ClienteService clienteService) {
		this.documentacaoRepository = documentacaoRepository;
		this.clienteService = clienteService;
	}

	public void save(Documentacao documentacao) {
		Cliente cliente = this.clienteService.findByCpfCnpj(documentacao.getCliente().getCpfCnpj());
		documentacao.setCliente(cliente);

		this.documentacaoRepository.save(documentacao);
	}

}
