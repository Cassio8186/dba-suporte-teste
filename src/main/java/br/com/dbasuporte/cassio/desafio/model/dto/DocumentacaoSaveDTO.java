package br.com.dbasuporte.cassio.desafio.model.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModelProperty;

public class DocumentacaoSaveDTO {

	@ApiModelProperty(name = "CPF ou CNPJ", notes = "CPF ou CNPJ do cliente possuidor do documento", required = true, example = "\"46781563050\"")
	@NotNull(message = "CPF ou CNPJ deve ser informado")
	@Length(min = 11, max = 14, message = "CPF/CNPJ deve possuir de 11 a 14 caracteres")
	private String cpfCnpj;

	@ApiModelProperty(name = "Tipo do Documento", required = true, example = "RG")
	@NotNull(message = "Tipo do Documento deve ser informado")
	@NotEmpty(message = "Tipo do Documento deve ser informado")
	private String tipoDocumento;

	private DocumentacaoSaveDTO(Builder builder) {
		this.cpfCnpj = builder.cpfCnpj;
		this.tipoDocumento = builder.tipoDocumento;
	}

	public DocumentacaoSaveDTO() {
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public String toString() {
		return "DocumentacaoSaveDTO [cpfCnpj=" + cpfCnpj + ", tipoDocumento=" + tipoDocumento + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		result = prime * result + ((tipoDocumento == null) ? 0 : tipoDocumento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentacaoSaveDTO other = (DocumentacaoSaveDTO) obj;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		if (tipoDocumento == null) {
			if (other.tipoDocumento != null)
				return false;
		} else if (!tipoDocumento.equals(other.tipoDocumento))
			return false;
		return true;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private String cpfCnpj;
		private String tipoDocumento;

		private Builder() {
		}

		public Builder cpfCnpj(String cpfCnpj) {
			this.cpfCnpj = cpfCnpj;
			return this;
		}

		public Builder tipoDocumento(String tipoDocumento) {
			this.tipoDocumento = tipoDocumento;
			return this;
		}

		public DocumentacaoSaveDTO build() {
			return new DocumentacaoSaveDTO(this);
		}
	}

}