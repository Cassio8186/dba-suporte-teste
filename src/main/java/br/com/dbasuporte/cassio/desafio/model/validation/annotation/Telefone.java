package br.com.dbasuporte.cassio.desafio.model.validation.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import br.com.dbasuporte.cassio.desafio.model.validation.validator.TelefoneValidator;

/**
 * 
 * @author Clairton Luz - clairton.c.l@gmail.com
 * 
 *         <para>
 * 
 *         <a href="https://clairtonluz.github.io/blog/2014/07/2014072299.html">
 *         fonte </a>
 * 
 *         </para>
 */
@Constraint(validatedBy = { TelefoneValidator.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface Telefone {

	String message() default "Telefone inválido Exemplo válido: (+5591989578989 ou +559294612145)";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}