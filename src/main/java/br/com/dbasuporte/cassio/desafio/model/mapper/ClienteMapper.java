package br.com.dbasuporte.cassio.desafio.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.dbasuporte.cassio.desafio.model.Cliente;
import br.com.dbasuporte.cassio.desafio.model.dto.ClienteDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {})
public interface ClienteMapper extends EntityMapper<Cliente, ClienteDTO> {
	ClienteMapper INSTANCE = Mappers.getMapper(ClienteMapper.class);
}
