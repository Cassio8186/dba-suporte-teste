package br.com.dbasuporte.cassio.desafio.model;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "documentacao")
public class Documentacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "id_documentacao")
	private int idDocumentacao;

	@JoinColumn(nullable = false, name = "id_cliente", foreignKey = @ForeignKey(name = "fk_documentacao_id_cliente_cliente"))
	@ManyToOne(fetch = FetchType.LAZY)
	private Cliente cliente;

	@Column(nullable = false, name = "tipo_documento", length = 50)
	private String tipoDocumento;

	@Column(nullable = false, name = "documento")
	private byte[] documento;

	private Documentacao(Builder builder) {
		this.idDocumentacao = builder.idDocumentacao;
		this.cliente = builder.cliente;
		this.tipoDocumento = builder.tipoDocumento;
		this.documento = builder.documento;
	}

	public Documentacao() {
	}

	public int getIdDocumentacao() {
		return idDocumentacao;
	}

	public void setIdDocumentacao(int idDocumentacao) {
		this.idDocumentacao = idDocumentacao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public byte[] getDocumento() {
		return documento;
	}

	public void setDocumento(byte[] documento) {
		this.documento = documento;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "Documentacao [idDocumentacao=" + idDocumentacao + ", cliente=" + cliente + ", tipoDocumento="
				+ tipoDocumento + ", documento="
				+ (documento != null ? Arrays.toString(Arrays.copyOf(documento, Math.min(documento.length, maxLen)))
						: null)
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + Arrays.hashCode(documento);
		result = prime * result + idDocumentacao;
		result = prime * result + ((tipoDocumento == null) ? 0 : tipoDocumento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documentacao other = (Documentacao) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (!Arrays.equals(documento, other.documento))
			return false;
		if (idDocumentacao != other.idDocumentacao)
			return false;
		if (tipoDocumento == null) {
			if (other.tipoDocumento != null)
				return false;
		} else if (!tipoDocumento.equals(other.tipoDocumento))
			return false;
		return true;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private int idDocumentacao;
		private Cliente cliente;
		private String tipoDocumento;
		private byte[] documento;

		private Builder() {
		}

		public Builder idDocumentacao(int idDocumentacao) {
			this.idDocumentacao = idDocumentacao;
			return this;
		}

		public Builder cliente(Cliente cliente) {
			this.cliente = cliente;
			return this;
		}

		public Builder tipoDocumento(String tipoDocumento) {
			this.tipoDocumento = tipoDocumento;
			return this;
		}

		public Builder documento(byte[] documento) {
			this.documento = documento;
			return this;
		}

		public Documentacao build() {
			return new Documentacao(this);
		}
	}

}
