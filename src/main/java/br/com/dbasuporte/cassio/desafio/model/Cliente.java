package br.com.dbasuporte.cassio.desafio.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "id_cliente")
	private int idCliente;

	@Column(nullable = false, name = "cpf_cnpj", length = 14)
	private String cpfCnpj;

	@Column(nullable = false, name = "nome_completo", length = 50)
	private String nomeCompleto;

	@Column(nullable = false, name = "email", length = 500)
	private String email;

	@Column(nullable = false, name = "telefone", length = 14)
	private String telefone;

	@OneToMany(mappedBy = "cliente", cascade = { CascadeType.REMOVE })
	private List<Documentacao> documentacoes = new ArrayList<Documentacao>();

	private Cliente(Builder builder) {
		this.idCliente = builder.idCliente;
		this.cpfCnpj = builder.cpfCnpj;
		this.nomeCompleto = builder.nomeCompleto;
		this.email = builder.email;
		this.telefone = builder.telefone;
		this.documentacoes = builder.documentacoes;
	}

	public Cliente() {
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public List<Documentacao> getDocumentacoes() {
		return documentacoes;
	}

	public void setDocumentacoes(List<Documentacao> documentacoes) {
		this.documentacoes = documentacoes;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "Cliente [idCliente=" + idCliente + ", cpfCnpj=" + cpfCnpj + ", nomeCompleto=" + nomeCompleto
				+ ", email=" + email + ", telefone=" + telefone + ", documentacoes="
				+ (documentacoes != null ? documentacoes.subList(0, Math.min(documentacoes.size(), maxLen)) : null)
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		result = prime * result + ((documentacoes == null) ? 0 : documentacoes.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + idCliente;
		result = prime * result + ((nomeCompleto == null) ? 0 : nomeCompleto.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		if (documentacoes == null) {
			if (other.documentacoes != null)
				return false;
		} else if (!documentacoes.equals(other.documentacoes))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (idCliente != other.idCliente)
			return false;
		if (nomeCompleto == null) {
			if (other.nomeCompleto != null)
				return false;
		} else if (!nomeCompleto.equals(other.nomeCompleto))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private int idCliente;
		private String cpfCnpj;
		private String nomeCompleto;
		private String email;
		private String telefone;
		private List<Documentacao> documentacoes = Collections.emptyList();

		private Builder() {
		}

		public Builder idCliente(int idCliente) {
			this.idCliente = idCliente;
			return this;
		}

		public Builder cpfCnpj(String cpfCnpj) {
			this.cpfCnpj = cpfCnpj;
			return this;
		}

		public Builder nomeCompleto(String nomeCompleto) {
			this.nomeCompleto = nomeCompleto;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder telefone(String telefone) {
			this.telefone = telefone;
			return this;
		}

		public Builder documentacoes(List<Documentacao> documentacoes) {
			this.documentacoes = documentacoes;
			return this;
		}

		public Cliente build() {
			return new Cliente(this);
		}
	}

}
