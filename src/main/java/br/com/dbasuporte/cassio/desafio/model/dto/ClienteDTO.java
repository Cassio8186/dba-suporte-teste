package br.com.dbasuporte.cassio.desafio.model.dto;

public class ClienteDTO {

	private int idCliente;

	private String cpfCnpj;

	private String nomeCompleto;

	private String email;

	private String telefone;

	private ClienteDTO(Builder builder) {
		this.idCliente = builder.idCliente;
		this.cpfCnpj = builder.cpfCnpj;
		this.nomeCompleto = builder.nomeCompleto;
		this.email = builder.email;
		this.telefone = builder.telefone;
	}

	public ClienteDTO() {
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@Override
	public String toString() {
		return "ClienteDTO [idCliente=" + idCliente + ", cpfCnpj=" + cpfCnpj + ", nomeCompleto=" + nomeCompleto
				+ ", email=" + email + ", telefone=" + telefone + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + idCliente;
		result = prime * result + ((nomeCompleto == null) ? 0 : nomeCompleto.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteDTO other = (ClienteDTO) obj;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (idCliente != other.idCliente)
			return false;
		if (nomeCompleto == null) {
			if (other.nomeCompleto != null)
				return false;
		} else if (!nomeCompleto.equals(other.nomeCompleto))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private int idCliente;
		private String cpfCnpj;
		private String nomeCompleto;
		private String email;
		private String telefone;

		private Builder() {
		}

		public Builder idCliente(int idCliente) {
			this.idCliente = idCliente;
			return this;
		}

		public Builder cpfCnpj(String cpfCnpj) {
			this.cpfCnpj = cpfCnpj;
			return this;
		}

		public Builder nomeCompleto(String nomeCompleto) {
			this.nomeCompleto = nomeCompleto;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder telefone(String telefone) {
			this.telefone = telefone;
			return this;
		}

		public ClienteDTO build() {
			return new ClienteDTO(this);
		}
	}

}
