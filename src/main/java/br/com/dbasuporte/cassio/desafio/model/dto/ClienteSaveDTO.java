package br.com.dbasuporte.cassio.desafio.model.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.dbasuporte.cassio.desafio.model.validation.annotation.CpfCnpj;
import br.com.dbasuporte.cassio.desafio.model.validation.annotation.Telefone;
import io.swagger.annotations.ApiModelProperty;

public class ClienteSaveDTO {

	@ApiModelProperty(name = "CPF ou CNPJ", required = true, example = "\"46781563050\"")
	@NotNull(message = "CPF ou CNPJ deve ser informado")
	@CpfCnpj
	private String cpfCnpj;

	@ApiModelProperty(name = "Nome Completo", required = true, example = "Jorge Caetano Veloso")
	@NotNull(message = "Nome Completo deve ser informado")
	@NotEmpty(message = "Nome Completo deve ser informado")
	@Length(max = 50, message = "Nome Completo não deve exceder 50 caracteres")
	private String nomeCompleto;

	@ApiModelProperty(name = "Email", required = true, example = "jorge@hotmail.com")
	@NotNull(message = "Email deve ser informado")
	@NotEmpty(message = "Email deve ser informado")
	@Length(max = 500, message = "Tamanho do email deve ser menor que 500 caracteres")
	@Email
	private String email;

	@ApiModelProperty(name = "telefone", required = true, example = "+5591458798451")
	@NotNull(message = "Telefone deve ser informado")
	@Length(min = 13, max = 14, message = "Telefone deve possuir 14 caracteres")
	@Telefone
	private String telefone;

	private ClienteSaveDTO(Builder builder) {
		this.cpfCnpj = builder.cpfCnpj;
		this.nomeCompleto = builder.nomeCompleto;
		this.email = builder.email;
		this.telefone = builder.telefone;
	}

	public ClienteSaveDTO() {
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@Override
	public String toString() {
		return "ClienteSaveDTO [cpfCnpj=" + cpfCnpj + ", nomeCompleto=" + nomeCompleto + ", email=" + email
				+ ", telefone=" + telefone + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((nomeCompleto == null) ? 0 : nomeCompleto.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteSaveDTO other = (ClienteSaveDTO) obj;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (nomeCompleto == null) {
			if (other.nomeCompleto != null)
				return false;
		} else if (!nomeCompleto.equals(other.nomeCompleto))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private String cpfCnpj;
		private String nomeCompleto;
		private String email;
		private String telefone;

		private Builder() {
		}

		public Builder cpfCnpj(String cpfCnpj) {
			this.cpfCnpj = cpfCnpj;
			return this;
		}

		public Builder nomeCompleto(String nomeCompleto) {
			this.nomeCompleto = nomeCompleto;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder telefone(String telefone) {
			this.telefone = telefone;
			return this;
		}

		public ClienteSaveDTO build() {
			return new ClienteSaveDTO(this);
		}
	}

}
