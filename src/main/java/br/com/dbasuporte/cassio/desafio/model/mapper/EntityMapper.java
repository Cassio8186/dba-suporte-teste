package br.com.dbasuporte.cassio.desafio.model.mapper;

import java.util.List;

/**
 * 
 * @param <E> - Entity
 * @param <D> - DTO
 */
public interface EntityMapper<E, D> {

	D toDTO(E entity);

	E toEntity(D dto);

	List<D> toDTO(List<E> entities);

	List<E> toEntity(List<D> dtos);

}
