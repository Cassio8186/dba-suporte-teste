package br.com.dbasuporte.cassio.desafio.controller.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import br.com.dbasuporte.cassio.desafio.exception.EntityAlreadyExistsException;
import br.com.dbasuporte.cassio.desafio.exception.EntityNotFoundException;
import br.com.dbasuporte.cassio.desafio.exception.MediaTypeNotAcceptedException;
import br.com.dbasuporte.cassio.desafio.model.dto.ResponseDTO;

@RestControllerAdvice
class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(EntityNotFoundException.class)
	protected ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex) {
		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.BAD_REQUEST.value())
				.mensagem(ex.getMessage())
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(EntityAlreadyExistsException.class)
	protected ResponseEntity<Object> handleEntityAlreadyExistsException(EntityAlreadyExistsException ex) {
		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.BAD_REQUEST.value())
				.mensagem(ex.getMessage())
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	protected ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.mensagem(ex.getMessage())
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});

		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.BAD_REQUEST.value())
				.mensagem(errors.toString())
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String errorMessage = String.format("Método Http %s não suportado para este url", ex.getMethod());

		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.METHOD_NOT_ALLOWED.value())
				.mensagem(errorMessage)
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(InvalidFormatException.class)
	protected ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex) {
		String expectedType = ex.getTargetType().getSimpleName();

		String errorMessage = expectedType == null ? ex.getMessage()
				: String.format("Erro na desserialização do valor %s, tipo esperado: %s", ex.getValue(),
						expectedType);

		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.mensagem(errorMessage)
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(JpaSystemException.class)
	protected ResponseEntity<Object> handleDataIntegrityViolationException(JpaSystemException ex) {
		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.mensagem(ex.getMessage())
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ExceptionHandler(MediaTypeNotAcceptedException.class)
	protected ResponseEntity<Object> handleMediaTypeNotAcceptedException(MediaTypeNotAcceptedException ex) {
		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.BAD_REQUEST.value())
				.mensagem(ex.getMessage())
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		if (ex.getCause() instanceof InvalidFormatException) {
			InvalidFormatException invalidFormatException = (InvalidFormatException) ex.getCause();
			return handleInvalidFormatException(invalidFormatException);
		}
		return super.handleHttpMessageNotReadable(ex, headers, status, request);

	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		String errorMessage = String.format("Erro interno: detalhes:  %s", ex.getMessage());
		ResponseDTO response = ResponseDTO.builder()
				.codigoRetorno(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.mensagem(errorMessage)
				.build();

		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
