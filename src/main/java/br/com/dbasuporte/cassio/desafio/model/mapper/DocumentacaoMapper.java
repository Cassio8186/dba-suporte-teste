package br.com.dbasuporte.cassio.desafio.model.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.dbasuporte.cassio.desafio.model.Documentacao;
import br.com.dbasuporte.cassio.desafio.model.dto.DocumentacaoDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {})
public interface DocumentacaoMapper extends EntityMapper<Documentacao, DocumentacaoDTO> {

	DocumentacaoMapper INSTANCE = Mappers.getMapper(DocumentacaoMapper.class);

	@Mapping(source = "cliente.cpfCnpj", target = "cpfCnpj")
	DocumentacaoDTO toDTO(Documentacao entity);

	@InheritInverseConfiguration
	Documentacao toEntity(DocumentacaoDTO dto);

}
